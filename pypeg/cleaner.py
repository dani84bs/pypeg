# -*- coding: utf-8 -*-

#The MIT License (MIT)
#
#Copyright (c) 2013 Daniele Trainini
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import struct

app_markers = ['\xff\xe1', '\xff\xe2', '\xff\xe3', '\xff\xe4', '\xff\xe5',
               '\xff\xe6', '\xff\xe7',
               '\xff\xe8', '\xff\xe9', '\xff\xea', '\xff\xeb', '\xff\xec',
               '\xff\xed', '\xff\xef', '\xff\xee',
]


class Cleaner(object):
    def __init__(self, img_path):
        with open(img_path, 'rb') as img_file:
            self._img_data = img_file.read()

    def _clean_marker(self, marker):
        position = self._img_data.find(marker)
        while position != -1:
            size = struct.unpack('>H',
                                 self._img_data[position + 2:position + 4])
            size = size[0] + 2
            self._img_data = (self._img_data[:position] +
                              self._img_data[position + size:])
            position = self._img_data.find(marker)

    def clean_all(self):
        for marker in app_markers:
            self._clean_marker(marker)

    def save(self, save_path):
        with open(save_path, 'wb') as save_img_file:
            save_img_file.write(self._img_data)



