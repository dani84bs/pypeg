# -*- coding: utf-8 -*-

#The MIT License (MIT)
#
#Copyright (c) 2013 Daniele Trainini
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import os
from PyQt4.QtGui import ( QMainWindow, QFileDialog,
                          QStandardItemModel, QStandardItem)

from pypeg import Cleaner

import layout


def _is_jpg(path):
    return path.endswith('.jpg') or path.endswith('.jpeg')


class RootWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(RootWindow, self).__init__(*args, **kwargs)
        self.ui = layout.Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.load.clicked.connect(self.load_files)
        self.ui.clean.clicked.connect(self.clean_files)
        self._loaded_directory = None
        self._mapping = {}


    def _scan_files(self, path):
        files = [os.path.join(self._loaded_directory, f) for f in
                 os.listdir(path)]
        return [f for f in files if os.path.isfile(f)]

    def load_files(self):
        self._loaded_directory = str(QFileDialog.getExistingDirectory(self))
        all_files = self._scan_files(self._loaded_directory)
        jpg_files = filter(_is_jpg, all_files)
        self._update_list_view(self.ui.display_list, jpg_files)

    def _update_list_view(self, list_view, entities):
        new_model = QStandardItemModel()
        for e in entities:
            entity_name = os.path.basename(e)
            self._mapping[entity_name] = e
            new_item = QStandardItem(entity_name)
            new_item.setEditable(False)
            new_model.appendRow(new_item)

        list_view.model()
        list_view.setModel(new_model)


    def clean_files(self):
        save_dir = str(QFileDialog.getExistingDirectory(self))
        for file in self._mapping:
            cleaner = Cleaner(self._mapping[file])
            cleaner.clean_all()
            cleaner.save(os.path.join(save_dir, file))
