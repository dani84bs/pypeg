# -*- coding: utf-8 -*-

#The MIT License (MIT)
#
#Copyright (c) 2013 Daniele Trainini
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import os

import pypeg

app_markers = pypeg.app_markers


class TestMarkers(unittest.TestCase):
    def setUp(self):
        with open('data/black.jpg', 'rb') as img_file:
            self.img_data = img_file.read()

    def _contain_markers(self, img_data):
        acc = 0
        for marker in app_markers:
            if img_data.find(marker) != -1:
                acc += 1
        return acc > 0

    def test_app_markers(self):
        res = self._contain_markers(self.img_data)
        self.assertTrue(res)

    def test_clean_markers(self):
        cleaner = pypeg.Cleaner('data/black.jpg')
        cleaner.clean_all()
        cleaner.save('black_stripped.jpg')

        with open('black_stripped.jpg', 'rb') as img_file:
            img_data = img_file.read()

        self.assertFalse(self._contain_markers(img_data))

    def tearDown(self):
        if os.path.exists('black_stripped.jpg'):
            os.remove(os.path.abspath('black_stripped.jpg'))


